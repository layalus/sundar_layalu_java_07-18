package com.cerotid.bank.model;


public class Account {
	private String accountType;


	public Account() {
		super();
		
	}



	public String printAccountInfo()
	{return accountType;}



	public String getAccountType() {
		return accountType;
	}



	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}



	@Override
	public String toString() {
		return "Account [accountType=" + accountType + "]";
	}




	
	
}
 