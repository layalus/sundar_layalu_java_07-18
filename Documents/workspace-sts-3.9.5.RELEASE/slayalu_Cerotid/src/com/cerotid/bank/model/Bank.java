package com.cerotid.bank.model;

import java.util.ArrayList;




public class Bank {
	private final String bankName = "Bank of Layalu";
	private ArrayList<Customer> customers; 
	

	public String getBankName() {
		return bankName;
	}
	// Accessor and Modifiers 

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public void printBankName () {
		System.out.println(getBankName());}
		
	public void printBankDetails()
	{
		System.out.println(toString());}

	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}
	
	
		
	}