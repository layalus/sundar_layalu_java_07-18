package com.cerotid.bank.model;

import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) {
		Bank bank = new Bank();
		
		Customer customer1 = new Customer();
		Customer customer2 = new Customer();
		Customer customer3 = new Customer();
		
		Account customer1act1 = new Account();
		customer1act1.setAccountType("Checking");
		Account customer1act2 = new Account();
		customer1act2.setAccountType("Business Checking");
		Account customer1act3 = new Account(); 
		customer1act3.setAccountType("saving");
		
		ArrayList<Account>customer1Accounts = new ArrayList<>();
		customer1Accounts.add(customer1act1);
		customer1Accounts.add(customer1act2);
		customer1Accounts.add(customer1act3);
		
		customer1.setAccounts(customer1Accounts);
		
		customer1.setAddress("111 Nepal St, VA");
		customer1.setCustomerName("Donald Trump");
	
		customer2.setAddress("123 Kathmandu St, VA");
		customer2.setCustomerName("Barak Trump");
		customer3.setAddress("11 Nepaldu St, VA");
		customer3.setCustomerName("Hari Bahadur");
		
		
		ArrayList <Customer> customerList = new ArrayList<>();
		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);
		
		bank.setCustomers(customerList);
		
		bank.printBankDetails();
		bank.printBankName();
		
		
		
		
		

		
	
		
	

	}

}
