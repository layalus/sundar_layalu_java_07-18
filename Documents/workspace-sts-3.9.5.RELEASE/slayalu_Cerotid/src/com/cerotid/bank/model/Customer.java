package com.cerotid.bank.model;

import java.util.ArrayList;

public class Customer {
	private String customerName; 
	private ArrayList <Account> accounts;
	private String address;
	
	

	public Customer() {
		super();
	}


	public Customer(String customerName, ArrayList<Account> accounts, String address) {
		super();
		this.customerName = customerName;
		this.accounts = accounts;
		this.address = address;
	}


	public Customer(ArrayList<Account> accounts) {
	super();
	this.accounts = accounts;
}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	//Accessors
	public ArrayList<Account> getAccounts() {
		return accounts;
	}

    //Mutator
	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}

	public String printCustomerDetails()
	{ return customerName + accounts + address;}
	
	public String printCustomerAccounts(){
		return address;}


	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", accounts=" + accounts + ", address=" + address + "]";
	}

	

}
